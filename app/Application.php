<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ApplicationEvent;
use App\ApplicationUserType;
use Hash;

class Application extends Model
{

    protected $table = 'applications';
 
    protected $fillable = [	
        'appid',
        'appkey',
        'secret_key',
        'domain',
        'created_at',
        'name',
        'description',
        'user_id',
        'slug',
        'status',
    ];

       
    public function events(){
        return $this->hasMany(ApplicationEvent::class, 'application_id');
    }

    public function channels(){
        return $this->hasMany(ApplicationUserType::class, 'application_id');
    }

    public function users(){
        return $this->hasMany(ChatUser::class, 'appid');
    }



}
