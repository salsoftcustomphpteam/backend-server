<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\MeetupParticipant;
use Hash;

class ApplicationEvent extends Model
{

    protected $table = 'application_events';
 
    protected $fillable = [	
        'application_id',
        'name'        
    ];

    public function userType(){
        return $this->belongsTo(ApplicationUserType::class, 'application_user_type_id');
    }
    
}
