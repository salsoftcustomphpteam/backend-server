<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\MeetupParticipant;
use Hash;

class ApplicationUserType extends Model
{

    protected $table = 'application_users_type';
 
    protected $fillable = [	
        'name',
        'application_id',      
    ];

    
}
