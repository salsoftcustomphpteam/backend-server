<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\FriendableTrait;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Meetup;


class ChatUser extends Authenticatable implements JWTSubject
{

    use FriendableTrait;

    protected $table = 'chat_users';
    
    protected $primaryKey = 'id';

    //protected $with = ['lastmessage'];

    protected $fillable = [
        'appid', 'uid', 'name', 'image', 'socketid', 'online', 'user_type'
    ];

    public static function findByUID($appid, $id){
        return self::where('appid', $appid)->where('uid', $id)->first();
    }

    public function latestMessage() {
        return $this->hasOne('App\Message', 'toid')->orderBy('id','desc');
    }
    public function mylatestMessage() {
        return $this->hasOne('App\Message', 'fromid')->orderBy('id','desc');
    }
    
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
 
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function meetup(){
        return $this->hasMany(Meetup::class, 'user_id');
    }

}






