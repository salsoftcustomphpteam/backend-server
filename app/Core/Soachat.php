<?php

namespace App\Core;

// namespace App;

class Soachat
{
    /*  Declearing The static variables */
    /*  Enter the your app id you obtained while registration */
    /*  Enter the your secret_key you obtained while registration */
    /*  Never show or reveal your secret_key */

    static $app_id = '24ecd1da6c1285ceaec6c2b5bca504d4';
    static $secret_key = '$2y$10$e3ncBWHD5yaAlHLdtjtATeeeZRBCZaKDRDi44v71OZT7b0aJGn7DG';
    static $endpoint = 'http://dev28.onlinetestingserver.com/soachatcentralizedWeb';
    static $feathers_endpoint = 'http://dev71.onlinetestingserver.com:4000';

    
    /* A curl Helper to perform HTTP POST */
    public static function request($fields, $url=''){
        $data_string = json_encode($fields);

        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                         'Content-Type: application/json',
                         'Content-Length: ' . strlen($data_string))
        );
        $response = curl_exec ( $ch );
        curl_close ( $ch );
        return $response;
    } 
    
   
    /*  Endpoint to add User to Soachat system database POST - /api/user/add */
    public static function registerService($payload=null){

        $data = $payload;        
        $url = self::$feathers_endpoint . '/registerService';
        $response = self::request($data, $url);
        return $response;
    }   
   
    /*  Endpoint to add User to Soachat system database POST - /api/user/add */
    public static function addUser($id,$name,$avatar=null,$isVideoIncluded=0){

        $data = array(
            'appid' =>  self::$app_id,
            'secret_key' =>  self::$secret_key,
            'id' =>  $id,
            'name' =>  $name,
            'isVideoIncluded' => $isVideoIncluded,
            'avatar' =>  $avatar,
        );
        $url = self::$endpoint . '/api/user/add';
        $response = self::request($data, $url );
        return $response;
    }

    /*  Endpoint to add User in bulk to Soachat system database POST - /api/user/add */
    public function addUsersBulk($users=[]){
        $data = array(
            'appid' =>  self::$app_id,
            'secret_key' =>  self::$secret_key,
            'user' =>  $users,
        );
        $url = self::$endpoint . '/api/user/bulk-add';
        $response = self::request($data, $url );
        return $response;
    }

    
    /*  Endpoint to update User to Soachat system database POST - /api/user/update */
    public static function updateUser($id,$name,$avatar=null,$isVideoIncluded=0){

        $data = array(
            'appid' =>  self::$app_id,
            'secret_key' =>  self::$secret_key,
            'id' =>  $id,
            'name' =>  $name,
            'isVideoIncluded' => $isVideoIncluded,
            'avatar' =>  $avatar,
        );
        $url = self::$endpoint . '/api/user/update';
        $response = self::request($data, $url );
        return $response;
    }
    
    /*  Endpoint to delete User from Soachat system database POST - /api/user/remove */
    public static function removeUser($id){

        $data = array(
            'appid' =>  self::$app_id,
            'secret_key' =>  self::$secret_key,
            'id' =>  $id,
        );
        $url = self::$endpoint . '/api/user/remove';
        $response = self::request($data, $url );
        return $response;
    }
    
    /*  Endpoint to retrieve User from Soachat system database POST - /api/user/remove */
    public static function reviveUser($id){

        $data = array(
            'appid' =>  self::$app_id,
            'secret_key' =>  self::$secret_key,
            'id' =>  $id,
        );
        $url = self::$endpoint . '/api/user/revive';
        $response = self::request($data, $url );
        return $response;
    }
    
    /*  Endpoint to add User friends to Soachat system database POST - /api/user/add-friends */

    public static function addFriendsBulk($friends = []){
        $data = array(
            'appid' =>  self::$app_id,
            'secret_key' =>  self::$secret_key,
            'friend' => $friends,
            // 'fromid' =>  $fromid,
            // 'toid' =>  $toid,
        );
        $url = self::$endpoint . '/api/user/add-friends-bulk';
        $response = self::request($data, $url );
        return $response;
    }


    public static function addFriends($fromid,$toid){
        $data = array(
            'appid' =>  self::$app_id,
            'secret_key' =>  self::$secret_key,
            'fromid' =>  $fromid,
            'toid' =>  $toid,
        );
        $url = self::$endpoint . '/api/user/add-friends';
        $response = self::request($data, $url );
        return $response;
    }
    
    
    /*  Endpoint to remove User friends from Soachat system database POST - /api/user/remove-friends */
    public static function removeFriends($fromid,$toid){

        $data = array(
            'appid' =>  self::$app_id,
            'secret_key' =>  self::$secret_key,
            'fromid' =>  $fromid,
            'toid' =>  $toid,
        );
        $url = self::$endpoint . '/api/user/remove-friends';
        $response = self::request($data, $url );
        return $response;
    }
    
    /*  Endpoint to Emit Event From Soachat-Feather Server system POST - /{serviceName} */
    public static function emitEvent($event, $channel, $payload=null){

        $data = [
            'event' => $event,
            'channel' => $channel,
            'payload' => $payload
        ]; 

        //$url = self::$feathers_endpoint .'/'. self::$service_name;
        $url = "http://dev71.onlinetestingserver.com:4000/sentinelsos";
        $response = self::request($data, $url);
        return [$response];
    }   


}    