<?php

namespace App;
use DB;
use Session;
use DateTime;
use DateTimeZone;

class Helper
{

    public static function getUsersInfo($id){
        return DB::table('users')->where('id',$id)->first();
    }

    public static function getUsersInfoByEmail($email){
        return DB::table('users')->where('email',$email)->first();
    }

    public static function random_color(){
       $colors =  array('#e5213b','#fd612c','#fd9a00','#a4cf30','#20aaea','#7a6ff0','#aa62e3','#e362e3','#8da3a6','#37c5ab','#eec300');
       return $colors[array_rand($colors,1)];
    }

    public static function getInitials($name, $img){
        $color = substr(self::random_color(),1);
        if(!$img){
            return 'https://ui-avatars.com/api/?rounded=true&background='.$color.'&color=fff&name='.$name;
        }
        else{
            return asset($img);
        }
    }

    public static function file_url_contents($url){
        if (!function_exists('curl_init')){
            die('CURL is not installed!');
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    public static function datetimeStamp($convertDate='',$convertToZone='',$converFromZone='', $convertDateFormat="Y-m-d h:i:s A", $returnDateFormat="Y-m-d h:i:s A"){
        $res = '';
        if(!$convertDate)
            $convertDate = date("Y-m-d h:i:s A");
        if($convertToZone==''){
            $whitelist = array(
                '127.0.0.1',
                '::1'
            );

            if(!in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
                $ipAddress = self::getRealIpAddr();
            }
            else
            {
                $ipAddress = self::file_url_contents('https://api.ipify.org/');
            }

            //$ipInfo = file_get_contents('http://ip-api.com/json/' . $ipAddress);
            $context = stream_context_create(array('http' => array('header'=>'Connection: close\r\n')));
            //$ipInfo = file_get_contents("http://ip-api.com/json/$ipAddress",false,$context);
            $ipInfo = self::file_url_contents("https://ipapi.co/$ipAddress/timezone/",false,$context);
            //echo $ipInfo;exit;
            //$ipInfo = json_decode($ipInfo);
            //$convertToZone = $ipInfo->timezone;
            $convertToZone = $ipInfo;
            // This is just an example. In application this will come from Javascript (via an AJAX or something)
            //$timezone_offset_minutes = $_REQUEST['timezone'];  // $_GET['timezone_offset_minutes']

            // Convert minutes to seconds
            //$timezone_name = timezone_name_from_abbr("", $timezone_offset_minutes*60, false);

            // Asia/Kolkata
            //echo 'timezone:'.$timezone_name;exit;
            //$convertToZone = $timezone = $_SESSION['time'];
            //echo 'convert to zone:'.$convertToZone;exit;

            //$convertToZone = '';
        }
        if($converFromZone==''){
            $converFromZone = date_default_timezone_get();
        }

        if(!in_array($converFromZone, timezone_identifiers_list())) { // check source timezone
            trigger_error(__FUNCTION__ . ': Invalid source timezone ' . $converFromZone, E_USER_ERROR);
        } elseif(!in_array($convertToZone, timezone_identifiers_list())) { // check destination timezone
            trigger_error(__FUNCTION__ . ': Invalid destination timezone ' . $convertToZone, E_USER_ERROR);
        } else {
            // create DateTime object
            $d = DateTime::createFromFormat($convertDateFormat, $convertDate, new DateTimeZone($converFromZone));
            // check source datetime
            if($d && DateTime::getLastErrors()["warning_count"] == 0 && DateTime::getLastErrors()["error_count"] == 0) {
                // convert timezone
                $d->setTimeZone(new DateTimeZone($convertToZone));
                // convert dateformat
                $res = $d->format($returnDateFormat);
            } else {
                trigger_error(__FUNCTION__ . ': Invalid source datetime ' . $convertDate . ', ' . $convertDateFormat, E_USER_ERROR);
            }
        }
        return $res;
    }

    public static function getRealIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
        {
            $ip=$_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
        {
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

}
