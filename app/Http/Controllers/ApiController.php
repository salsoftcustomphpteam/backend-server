<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Helper;
use App\User;
use App\Message;
use App\ChatUser;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use File;
use App\Log;
use App\Application;
use Validator;
use Hash;


class ApiController extends Controller
{

    public $loginAfterSignUp = true;

    public function __construct(){
        //require(__DIR__.'/../../Encryption.php');
    }
    
    public function register(Request $request){
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6'],
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        $token = JWTAuth::fromUser($user);        

        return response()->json([
            "code" => 200,
            "message" => 'User is registered successfully',
            "type" => "Success",
            "data" => $user,
            "token" => $token,
        ]);        

    }
    public function apiLogin(Request $request){
        $user = ChatUser::where('email', $request->email);

        // if (auth()->guard('admin')->attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
        //     $user = auth()->guard('admin')->user();
        //     $token = JWTAuth::fromUser($user);        

        $token = JWTAuth::fromUser($user);        

        return response()->json([
            "code" => 200,
            "type" => "Success",
            "data" => $user,
            "token" => $token,
        ]);
    }


    public function login(Request $request){
        if(!$this->authenticateCredentials($request)){
            return response()->json([
                "code" => 500,
                "shortmessage" => "Invalid AppID or Secret Key",
                "type" => "Error",
                "message" => "The Appid or secret key you provided are incorrect",
            ]);
        }
        //find the user id 
        $user = ChatUser::findByUID($request->appid, $request->id);
        // authenticate via user id and return the response token
       
        //return $user;

        $token = JWTAuth::fromUser($user);        

        return response()->json([
            "code" => 200,
            "type" => "Success",
            "data" => $user,
            "token" => $token,
        ]);
    }

    public function logout(Request $request)
    {
        $this->validate($request, [
            'token' => 'required'
        ]);
 
        try {
            JWTAuth::invalidate($request->token);
 
            return response()->json([
                "code" => 200,
                'success' => true,
                'message' => 'User logged out successfully'
            ]);
        } catch (JWTException $exception) {
            return response()->json([
                "code" => 500,
                'success' => false,
                'message' => 'Sorry, the user cannot be logged out'
            ], 500);
        }
    }    
    
    public function profile(Request $request){
        return response()->json([
            "code" => 200,
            'success' => true,
            'data' =>  ['user' => $request->user(), 'friend' => $request->user()->getFriends()]
        ], 200);
    }

    /* Authenticate Credentials */
    public function authenticateCredentials(Request $request){
    	$chk = DB::table('applications')->where([
		    		'appid' => $request->appid,
		    		'secret_key' => $request->secret_key,
		    	])->get();        
		if(count($chk))
		    return 1;
		else
		    return 0;
    }

    public function getUnreadMessagesCount(Request $request){
    	$chk = DB::table('applications')->where([
		    		'appid' => $request->appid,
		    	])->get();        
		if(count($chk)){

            $user = DB::table('chat_users')
                        ->where('appid',$request->appid)
                        ->where('uid',$request->id)
                        ->first();
            if($user){
                $data = DB::table('messages')->where('toid',$user->id)->where('read',0)->count();
                return response()->json([
                    "code" => 200,
                    "type" => "Success",
                    "data" => $data,
                ]);
            }   
            else{
            return response()->json([
                "code" => 500,
                "shortmessage" => "Invalid or unrecognized user id",
                "type" => "Error",
                "message" => "The Appid or secret key you provided are incorrect or either user id is not recognized",
            ]);
                
            }
		    
		}
		else{
            return response()->json([
                "code" => 500,
                "shortmessage" => "Invalid AppID or Secret Key",
                "type" => "Error",
                "message" => "The Appid or secret key you provided are incorrect",
            ]);
		}

    }
    
    public function addUsersBulk(Request $request){
        if(!$this->authenticateCredentials($request)){
            return response()->json([
                "code" => 500,
                "shortmessage" => "Invalid AppID or Secret Key",
                "type" => "Error",
                "message" => "The Appid or secret key you provided are incorrect",
            ]);
        }

        $added_users = [];
        foreach($request->user as $u){
    		$data = $this->insertUserIfNotExist($request->appid, $u['id'], $u['name'], $u['avatar'], @$u['isVideoIncluded']);
    		if($data['operation'] == 'insert'){
                array_push($added_users, $data['uid']);
    		}
        }

	    return response()->json([
            "code" => 200,
            "shortmessage" => "The users are inserted",
            "type" => "Success",
            "users" => $added_users,
            "message" => "All users are inserted to our platfor, please update your flag on database",		        
	    ]);		    
    }

    /* Add User to the system */
    public function addUser(Request $request){
        if(!$this->authenticateCredentials($request)){
            return response()->json([
                "code" => 500,
                "shortmessage" => "Invalid AppID or Secret Key",
                "type" => "Error",
                "message" => "The Appid or secret key you provided are incorrect",
            ]);
        }
        
		$data = $this->updateChatUserElseInsert($request->appid, $request->id, $request->name, $request->avatar, $request->isVideoIncluded);
		if($data['operation'] == 'update'){
		    return response()->json([
                "code" => 200,
                "shortmessage" => "The user is updated",
                "type" => "Success",
                "message" => "Record already exists, Hence Updating the record",		        
		    ]);
		}
		else{
		    return response()->json([
                "code" => 200,
                "shortmessage" => "The user is inserted",
                "type" => "Success",
                "message" => "New user is added to the system",		        
		    ]);		    
		}
    }

    
    /* All User to the system */
    public function allUser(Request $request){
        if(!$this->authenticateCredentials($request)){
            return response()->json([
                "code" => 500,
                "shortmessage" => "Invalid AppID or Secret Key",
                "type" => "Error",
                "message" => "The Appid or secret key you provided are incorrect",
            ]);
        }
        
        if($request->paginate)           
            $data = DB::table('chat_users')->where('appid',$request->appid)->where('deleted',0)->paginate(20);
        else
            $data = DB::table('chat_users')->where('appid',$request->appid)->where('deleted',0)->get();

        return response()->json([
            "code" => 200,
            "data" => $data,
            "type" => "Success"
        ]);		    
    }
    
    /* Search User in the system */
    public function searchUser(Request $request){

        $me = $request->user();
        $users = ChatUser::where('appid',$request->appid)
                        ->where('name','LIKE','%'.$request->name.'%')
                        ->get();
        $users->map(function (&$user) use ($me) {
             return $user['isFriend'] = $user->isFriendWith($me);
        });
        return response()->json([
            "code" => 200,
            "data" => $users,
            "type" => "Success"
        ]);		    
    }    

    /* Add Update User in the system */
    public function updateUser(Request $request){
        if(!$this->authenticateCredentials($request)){
            return response()->json([
                "code" => 500,
                "shortmessage" => "Invalid AppID or Secret Key",
                "type" => "Error",
                "message" => "The Appid or secret key you provided are incorrect",
            ]);
        }
        
		$data = $this->updateChatUserElseInsert($request->appid, $request->id, $request->name, $request->avatar, $request->isVideoIncluded);
		if($data['operation'] == 'update'){
		    return response()->json([
                "code" => 200,
                "shortmessage" => "The user is updated",
                "type" => "Success",
                "message" => "User is updated to the system",		        
		    ]);	
		}
		else{
		    return response()->json([
                "code" => 200,
                "shortmessage" => "The user is inserted",
                "type" => "Success",
                "message" => "Record does not exists, Hence inserting the record",		        
		    ]);
	    
		}
    }
    
     /* remove User from the system */
    public function removeUser(Request $request){
        if(!$this->authenticateCredentials($request)){
            return response()->json([
                "code" => 500,
                "shortmessage" => "Invalid AppID or Secret Key",
                "type" => "Error",
                "message" => "The Appid or secret key you provided are incorrect",
            ]);
        }
        // we will not delete user from the system instead set the flag to 1 
        $d = DB::table('chat_users')->where('appid',$request->appid)->where('uid',$request->id)->update(['deleted'=>1]);
	    if($d){
    	    return response()->json([
                "code" => 200,
                "shortmessage" => "The user is deleted",
                "type" => "Success",
                "message" => "The user is deleted from the system",		        
    	    ]);
	    }

        return response()->json([
            "code" => 500,
            "shortmessage" => "Invalid user id",
            "type" => "Error",
            "message" => "The user id you provided is incorrect",
        ]);    
    }

    public function reviveUser(Request $request){
        if(!$this->authenticateCredentials($request)){
            return response()->json([
                "code" => 500,
                "shortmessage" => "Invalid AppID or Secret Key",
                "type" => "Error",
                "message" => "The Appid or secret key you provided are incorrect",
            ]);
        }
        // we will not delete user from the system instead set the flag to 1 
        $d = DB::table('chat_users')->where('appid',$request->appid)->where('uid',$request->id)->update(['deleted'=>0]);
	    if($d){
    	    return response()->json([
                "code" => 200,
                "shortmessage" => "The user is revived back",
                "type" => "Success",
                "message" => "The user which was deleted has been revieved back",		        
    	    ]);
	    }

        return response()->json([
            "code" => 500,
            "shortmessage" => "Invalid user id",
            "type" => "Error",
            "message" => "The user id you provided is incorrect",
        ]);    
    }
    
    /* Remove Friends */
    public function removeFriends(Request $request){
        if(!$this->authenticateCredentials($request)){
            return response()->json([
                "code" => 500,
                "shortmessage" => "Invalid AppID or Secret Key",
                "type" => "Error",
                "message" => "The Appid or secret key you provided are incorrect",
            ]);
        }
        
        $from_id = $this->getUserId($request->appid, $request->fromid);
        $toid = $this->getUserId($request->appid, $request->toid);
        
        if(!$fromid || !$toid){
            return response()->json([
                "code" => 500,
                "shortmessage" => "FromId or ToId are invalid",
                "type" => "Error",
                "message" => "From Id or To Id you provided does not exist in our system, Please try to add the users in our system before performing this api",
            ]);            
        }
        
        DB::table('friends')->where([
            ['fromid', '=', $fromid->id],
            ['toid', '=', $toid->id],
        ])->delete();
        
        DB::table('friends')->where([
            ['toid', '=', $fromid->id],
            ['fromid', '=', $toid->id],
        ])->delete();
                
        return response()->json([
            "code" => 200,
            "shortmessage" => "Friends are removed from the system",
            "type" => "Success",
            "message" => "Target Friends are successfully removed from the system",
        ]);      
                
    }

    /* Accept Friends */
    public function acceptFriendRequest(Request $request){

        $sender = $request->user();
        $recipient = ChatUser::find($request->toid);
        $sender->acceptFriendRequest($recipient);

        return response()->json([
            "code" => 200,
            "type" => "Success",
            "shortmessage" => "You have accept the friend request successfully",
            "messgae" => "You have accept the friend request successfully",
        ]);      
                
    }
    
    /* Accept Friends */
    public function rejectFriendRequest(Request $request){

        $sender = $request->user();
        $recipient = ChatUser::find($request->toid);
        $sender->denyFriendRequest($recipient);

        return response()->json([
            "code" => 200,
            "type" => "Success",
            "shortmessage" => "You have reject the friend request successfully",
            "messgae" => "You have reject the friend request successfully",

        ]);               
    }
    
    /* Accept Friends */
    public function unFriend(Request $request){
        // if(!$this->authenticateCredentials($request)){
        //     return response()->json([
        //         "code" => 500,
        //         "shortmessage" => "Invalid AppID or Secret Key",
        //         "type" => "Error",
        //         "message" => "The Appid or secret key you provided are incorrect",
        //     ]);
        // }
        
        $sender = ChatUser::findByUID($request->appid, $request->fromid);
        $recipient = ChatUser::findByUID($request->appid, $request->toid);
        $sender->unfriend($recipient);

        return response()->json([
            "code" => 200,
            "type" => "Success",
            "shortmessage" => "You have unfriend the participant successfully",
            "messgae" => "You have unfriend the participant successfully",
        ]);               
    }

    /* Add Friends */
    public function addFriends(Request $request){

        $sender = $request->user();
        $recipient = ChatUser::find($request->toid);
        $submitted = $sender->befriend($recipient);
        
        return response()->json([
            "code" => 200,
            "shortmessage" => "Friends are added to the system",
            "type" => "Success",
            "message" => "Target Friends are successfully added to the system",
        ]);                 
    }

    public function getFriendRequest($user){
        
        $data =  $user->getPendingFriendships()->pluck('sender_id');
        $record = ChatUser::whereIn('id', $data)->get();
        return $record;
    }

    public function getFriends(Request $request){
        
        $friendQuery = $request->user()->getFriends($perPage = 10);

        $arrPaginateData = $friendQuery->toArray();
    
        $friends = $arrPaginateData['data'];
        
        if($request->page == 1){
            
            $pendingFriendRequest = $this->getFriendRequest($request->user());

            if(count($pendingFriendRequest))
                array_push($friends, ...$pendingFriendRequest);
        }
    
        return response()->json([
            "code" => 200,
            "type" => "Success",
            "data" => ['data' => $friends]

        ]);         
    }

    /* Add Friends Bulk */
    public function addFriendsBulk(Request $request){
        if(!$this->authenticateCredentials($request)){
            return response()->json([
                "code" => 500,
                "shortmessage" => "Invalid AppID or Secret Key",
                "type" => "Error",
                "message" => "The Appid or secret key you provided are incorrect",
            ]);
        }
        foreach($request->friend as $f){
            $fromid = $this->getUserId($request->appid, $f['id']);
            if($fromid){

                DB::table('friends')
                ->where('fromid', $fromid->id)
                ->orWhere('toid', $fromid->id)
                ->delete();

                foreach($f['data'] as $d){
                    $toid = $this->getUserId($request->appid, $d['toid']);
                    
                    if($toid){
                        DB::table('friends')->insert([
                            ['fromid' => $fromid->id, 'toid' => $toid->id, 'created_at' => Helper::datetimeStamp()],
                            ['toid'   => $fromid->id, 'fromid' => $toid->id, 'created_at' => Helper::datetimeStamp()],
                        ]);        
                    }
                    
                }
            }
        }
        return response()->json([
            "code" => 200,
            "shortmessage" => "Friends are added to the system",
            "type" => "Success",
            "message" => "Friends are successfully added to the system",
        ]);                 
    }
    
    public function resyncFriendLists(Request $request){
        if(!$this->authenticateCredentials($request)){
            return response()->json([
                "code" => 500,
                "shortmessage" => "Invalid AppID or Secret Key",
                "type" => "Error",
                "message" => "The Appid or secret key you provided are incorrect",
            ]);
        }      
        $from = $this->getUserId($request->appid, $request->user_id);
        if(!$request->user_id || !$from){
            return response()->json([
                "code" => 500,
                "shortmessage" => "Invalid User id",
                "type" => "Error",
                "message" => "The user id you provided is incorrect",
            ]);
        }
        
        DB::table('friends')
        ->where('fromid', $from->id)
        ->orWhere('toid', $from->id)
        ->delete();

        foreach($request->friend as $d){
            $toid = $this->getUserId($request->appid, $d['toid']);
            if($toid){
                DB::table('friends')->insert([
                    ['fromid' => $from->id, 'toid' => $toid->id, 'created_at' => Helper::datetimeStamp()],
                    ['toid'   => $from->id, 'fromid' => $toid->id, 'created_at' => Helper::datetimeStamp()],
                ]);        
            }
        }    
        
    	return response()->json(['status' => 200, 'message' => 'The friend list is resynced.']);
    }
    
    public function getUserId($appid, $id){
		$u = DB::table('chat_users')->where([
	    		'appid' => $appid,
	    		'uid' => $id,
	    ])->first();
	    return $u;
    }

    /* Update User if exist else insert */
	function updateChatUserElseInsert($appid, $id, $name, $avatar, $isVideoIncluded=0){
		$u = DB::table('chat_users')->where([
	    		'appid' => $appid,
	    		'uid' => $id,
	    ])->first();
	    if($u){
			DB::table('chat_users')->where([
	    		'appid' => $appid,
	    		'uid' => $id,
		    ])->update([
		    	'name' => $name, 
		    	'image' => $avatar,
		    	'isVideoIncluded' => $isVideoIncluded,
		    	'updated_at' => Helper::datetimeStamp()
		    ]);
		    return array('id'=>$u->id, 'operation' => 'update');
	    }
	    else{
			$rec = DB::table('chat_users')->insertGetId([
				    		'appid' => $appid,
				    		'uid' => $id,
							'name' => $name, 
							'image' => $avatar,
							'isVideoIncluded' => $isVideoIncluded,
							'created_at' => Helper::datetimeStamp()		    		
					    ]);
		    return array('id'=>$rec, 'operation' => 'insert', 'uid'=>$id);
	    }
	}
	
    /* if not exist insert */
	function insertUserIfNotExist($appid, $id, $name, $avatar, $isVideoIncluded=0){
		$u = DB::table('chat_users')->where([
	    		'appid' => $appid,
	    		'uid' => $id,
	    ])->first();
	    if(!$u){
			$rec = DB::table('chat_users')->insertGetId([
				    		'appid' => $appid,
				    		'uid' => $id,
							'name' => $name, 
							'image' => $avatar,
							'isVideoIncluded' => $isVideoIncluded,
							'created_at' => Helper::datetimeStamp()		    		
					    ]);
		    return array('id'=>$rec, 'operation' => 'insert', 'uid'=>$id);
	    }else{
		    return array('operation' => 'rejected');
	    }
	}
		
    /*  Authenticate the environment to initate chat on client side*/
    public function authenticate(Request $request){
    	$chk = DB::table('applications')->where([
		    		'appid' => $request->appid,
		    		'appkey' => $request->appkey,
		    		'domain' => $request->domain,
		    	])->get();

    	if(!count($chk)){    		
    		return response()->json(['status' => 500, 'message' => 'The credentials are invalid, environment can not be initiated']);
    	}
    	else{
			$initator = $this->getUserId($request->appid, $request->id);
		    if(!$initator)
    			return response()->json(['status' => 500, 'message' => 'Invalid User ID']);		    	
		    else
		    	$userid = $initator->id;
		    $userid = encrypt_decrypt('encrypt', $userid);
    		return response()->json(['id'=>$userid, 'status' => 200, 'message' => 'The credentials are valid, initiating the environment']);
    	}
    }

    public function getChatMessages(Request $request){
        
        $myid = $request->user()->id;
        $toid = $request->toid;

        $messages = Message::where(function($q) use($myid, $toid) {
            $q->where('fromid',$myid)->where('toid', $toid);
        })
        ->orWhere(function ($q) use($myid, $toid) {
            $q->orWhere('toid',$myid)->where('fromid', $toid);
        })
        ->orderBy('id','desc')->paginate(20);   
                
        return response()->json([
            "code" => 200,
            "type" => "Success",
            "data" => $messages
        ]);          
    }
    
    public function hasFriendRequestFrom(Request $request){

        $sender = $request->user();
        $user = ChatUser::find($request->toid);
        
        $resp1 = $user->hasFriendRequestFrom($sender);
        $resp2 = $sender->hasFriendRequestFrom($user);
        
        return response()->json([
            "code" => 200,
            "type" => "Success",
            "data" => $resp2
        ]);          
    }
    
    public function saveChat(Request $request){
    
   
        $myid = $request->user()->id;
        $toid = $request->toid;
        $filename = $fname = '';

        if ($request->file) {
            // $file = $request->file('file');
            // $name = $file->getClientOriginalName();
            // $path = '/user-media/users/' . $request->user()->appid . '/'.$request->user().'/chat/';
            // $newFileName = $this->getFileName($path, $name);
            // $filepath  = $path . trim($newFileName);
            
            $image64 = $request->file;
            if (!empty($image64) || $image64 != '') {
                list($type, $image64) = explode(';', $image64);
                list(, $extension) = explode('/', $type);
                list(, $image64) = explode(',', $image64);
            }
    
            $path = '/user-media/' . $request->user()->appid . '/users/'.$request->user()->id.'/chat/';
            $directoryPath = public_path($path);
    
            if (File::isDirectory($directoryPath)) {
                //Perform storing
            } else {
                File::makeDirectory($directoryPath,0777,true);
                //Perform storing
            }

    
            $image64 = base64_decode($image64);
            $time = time();
            $path = $path . '/';
            $destination = public_path($path);
            $filename = md5(rand(11111111, 99999999)) . '.' . $extension;
            file_put_contents($destination . $filename, $image64);
            $fname = $path.$filename;

        }
        
        
        Message::create([
            'fromid' => $myid,
            'toid' => $toid,
            'content' => $request->content,
            'created_at' => $request->created_at,
            'read' => $request->read,
            'file' => $fname,
        ]);
        
        return response()->json([
            "code" => 200,
            "shortmessage" => "The chat is saved",
            "type" => "Success",
            "message" => "The chat is saved to system",	
        ]);	       
    }

    public function createApplication(Request $request){
        $user = request()->user();
        $id = DB::table('applications')->max('id');    
        if(!$id)
            $id = 1;
        else
            $id++;

        if(!$request->domain || !$request->name || !$request->description){
            return response()->json([
                "code" => 500,
                "message" => "Please fill the required fields",
            ], 500);
        }

        $appid = md5(date('Y-m-d H:i:s').'SALSOFTAPPID');
        $appkey = md5(date('Y-m-d H:i:s').'SALSOFTAPPKEY'); 
        $secret_key = hash::make(date('Y-m-d H:i:s').'SALSOFTSECRETKEY'); 

        DB::table('applications')->insert([
            'appid' => $appid,
            'appkey' => $appkey,
            'secret_key' => $secret_key,
            'domain' => $request->domain,
            'created_at' => Helper::datetimeStamp(),
            'name' => $request->name,
            'description' => $request->description,
            'user_id' => $user->id,
            'slug' => str_slug($request->name),
        ]);

        return response()->json([
            "code" => 200,
            "message" => "Application is created",
        ], 200);
        
    }
    
    public function myApplications(Request $request){
        $user = request()->user();
        $rec = DB::table('applications')->where('user_id', $user->id)->get();
        return response()->json([
            "code" => 200,
            "data" => $rec,
        ], 200);
    }

    public function applicationDetails(Request $request, $id){
        $user = request()->user();
        $rec = DB::table('applications')->where('user_id', $user->id)->where('id', $id)->get();
        return response()->json([
            "code" => 200,
            "data" => $rec,
        ], 200);
    }

    
    public function updateApplication(Request $request){
        $user = request()->user();
        $id = $request->id;
        $rec = DB::table('applications')->where('user_id', $user->id)->where('id',$id)->get();
        if(!count($rec)){
            return response()->json([
                "code" => 500,
                "message" => "No Record Found",
            ], 500);            
        }

        if(!$request->domain || !$request->name || !$request->description){
            return response()->json([
                "code" => 500,
                "message" => "Please fill the required fields",
            ], 500);
        }

        DB::table('applications')->where('id',$id)->update([
            'domain' => $request->domain,
            'updated_at' => Helper::datetimeStamp(),
            'name' => $request->name,
            'description' => $request->description,
            'slug' => str_slug($request->name),
        ]);

        return response()->json([
            "code" => 200,
            "message" => "Application is updated",
        ], 200);

    }

    public function applicationUsers(Request $request, $id){
        $user = request()->user();
        $rec = DB::table('applications')->where('user_id', $user->id)->where('id',$id)->first();
        $data = ChatUser::where('appid', $rec->id)->get();    
        return response()->json([
            "code" => 200,
            "data" => $data,
        ], 200);

    }

    public function addToLog(Request $request){
        Log::create([
            'payload' => $request->all(),
        ]);
        return response()->json([
            "code" => 200,
            "message" => 'Paylog is crearted',
        ], 200);        
    }

    public function getApps(Request $request){
        $rec = Application::all();
        $rec->load('events','channels','users');
        return $rec;        
    }

    public function saveLog(Request $request){
        Log::create([
            'payload' => $request->all()
        ]);
        return response()->json([
            "code" => 200,
            "message" => 'Log is saved',
        ], 200);        
    }

}
