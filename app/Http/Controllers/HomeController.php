<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use DB;
use Auth;
use Session;
use Redirect;
use App\Helper;
use Hash;
use App\Application;
use App\ApplicationEvent;
use App\ApplicationUserType;
use Validator;
use Illuminate\Support\Str;
use App\Core\Soachat;
use Exception;
use Symfony\Component\Debug\Debug;

class HomeController extends Controller
{
    public function index(){
        return view('home');
    }

    public function myApplication(){
        $rec = DB::table('applications')->where('user_id', Auth::user()->id)->get();
        return view('pages.applications.show',['rec' => $rec]);
    }

    public function editApplication($id){
        $id = Crypt::decryptString($id);
        $rec = DB::table('applications')->where('user_id', Auth::user()->id)->where('id',$id)->get();
        if(count($rec) > 0)
            return view('pages.applications.edit',['rec' => $rec[0]]);        
        else
            return redirect('/my-application');
    }

    public function detailApplication($id){
        $id = Crypt::decryptString($id);
        $rec = Application::find($id);
        $rec->load('events','channels','users');
        if($rec)
            return view('pages.applications.detail',['rec' => $rec]);        

        return redirect('/my-application');
    }

    public function submitApplication(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'domain' => 'required',
            'description' => 'required',
            'user_types' => 'required',
            'events' => 'required',            
        ]);
        if($validator->fails()){
            Session::flash('error',$validator->errors()->first());
            return redirect()->back();            
        }

        $appid = md5(date('Y-m-d H:i:s').'SALSOFTAPPID');
        $appkey = md5(date('Y-m-d H:i:s').'SALSOFTAPPKEY'); 
        $secret_key = hash::make(date('Y-m-d H:i:s').'SALSOFTSECRETKEY'); 

        $application = Application::create([
            'appid' => $appid,
            'appkey' => $appkey,
            'secret_key' => $secret_key,
            'domain' => $request->domain,
            'name' => $request->name,
            'description' => $request->description,
            'user_id' => Auth::user()->id,
            'slug' => str_slug($request->name),
        ]); 

        //user types is user channel
        foreach(explode(',',$request->user_types) as $type){
            $application->channels()->save(new ApplicationUserType([
                'name' => trim($type)
            ]));
        }
        
        foreach(explode(',',$request->events) as $event){
            $application->events()->save(new ApplicationEvent([
                'name' => trim($event)
            ]));
        }

        // $payload = Application::find(1);
        $application->load('events','channels','users');
        try{
            $response = Soachat::registerService($application);
        }catch(Exception $e){
            Debug::log($e);
        }

        // Soachat::registerService();
        Session::flash('success','Application is created');
        return redirect('my-application');

    }

    public function updateApplication(Request $request){
        $id = Crypt::decryptString($request->form_token_);
        $rec = DB::table('applications')->where('user_id', Auth::user()->id)->where('id',$id)->get();
        if(!count($rec))
           return redirect()->back();

        if(!$request->domain || !$request->name || !$request->description){
            Session::flash('error','Please fill the required fields');
            return redirect()->back();            
        }

        DB::table('applications')->where('id',$id)->update([
            'domain' => $request->domain,
            'updated_at' => Helper::datetimeStamp(),
            'name' => $request->name,
            'description' => $request->description,
            'slug' => str_slug($request->name),
        ]);

        Session::flash('message','Application is updated');
        return redirect()->back();

    }

    public function profile(Request $request){
        return view('pages.users.profile',['user' => Auth::user()]);
    }

    public function updateProfile(Request $request){

        $user_id = Crypt::decryptString($request->token_id);
        $inputs = [
            'name' => $request->name ,
            'telephone' => $request->phone ,
            'gender' => $request->gender ,
            'country' => $request->country ,
            'city' => $request->city ,
            'dob' => $request->dob ,
            'state' => $request->state ,
            'zipcode' => $request->zip_code ,
            'address' => $request->address ,
        ];
        if($request->password){
            $inputs['password'] = Hash::make($request->password);
        }

        \App\User::where('id', $user_id)->update($inputs);
        if($request->file('personal_img')){
            $file = $request->file('personal_img');
            $filename = $file->getClientOriginalName();
            $path = '/users/'.md5($user_id).'/';
            $destination = public_path($path);
            $file->move($destination,$filename);
            $user_image = $path.$filename;
            $image = $user_image;
            \App\User::where('id', $user_id)
                ->update(['image' => $image]);
        }


        Session::flash('message','User is updated');
        return redirect('/profile');

    }

}
