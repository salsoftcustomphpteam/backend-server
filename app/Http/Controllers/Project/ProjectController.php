<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use DB;
use Redirect;
use Validator;
use Auth;
use App\Helper;

class ProjectController extends Controller
{
    public function myProjects(Request $request)
    {
        $projects = DB::table('projects as p')
            ->join('users as u', 'u.id', '=', 'p.created_by')
            ->select('p.*','u.name','u.image as user_image')
            ->where('p.created_by', Auth::user()->id)
            ->get();
        return view('pages.project.my-projects', ['projects' => $projects]);
    }

    public function getThreadsConvo($id){
        return view('pages.project.thread-inner');
    }

    public function discussionThreads(Request $request){
        $threads = DB::table('threads as t')
                        ->join('projects as p','p.id','=','t.project_id')
                        ->select('t.*','p.*')
                        ->where('p.created_by',Auth::user()->id)->get();

        return view('pages.project.threads',['threads' => $threads]);
    }

    public function viewProject($id){
        $project = DB::table('projects as p')
            ->join('users as u', 'u.id', '=', 'p.created_by')
            ->select('p.*','u.name','u.image as user_image')
            ->where('p.created_by', Auth::user()->id)
            ->where('p.id', $id)
            ->first();
        return view('pages.project.view', ['project' => $project]);
    }

    public function getProjectUserInvites($id){
        $inv = DB::table('project_invites')->where('project_id',$id)->get();
        $str = '';
        foreach ($inv as $i){
            $user = \App\Helper::getUsersInfoByEmail($i->to_email);
            if($user)
                $str .= '<li class="text-primary"><i class="fa fa-dot-circle-o">&nbsp;&nbsp;</i> '.$user->name.'</li>';
            else
                $str .= '<li class="text-danger"><i class="fa fa-dot-circle-o">&nbsp;&nbsp;</i> '.$i->to_email.'</li>';
        }
        return $str;
    }

    public function submitProject(Request $request)
    {
        $id = Auth::user()->id;
        if (!$request->file('projectimage')) {
            Session::flash('error', 'Please attach an image for project');
            return redirect()->back()->withInput();
        }
        $validator = Validator::make(
            array('fileValidation' => $request->file('projectimage')),
            array('fileValidation' => 'required|mimes:png,jpg,jpeg,bmp')
        );
        if ($validator->fails()) {
            Session::flash('error', 'File type is not supported');
            return redirect()->back()->withInput();
        }

        // add Project
        $project_id = DB::table('projects')->insertGetId([
            'title' => $request->title,
            'status' => $request->status,
            'description' => $request->description,
            'created_at' => Helper::datetimeStamp(),
            'created_by' => $id,
        ]);

        DB::table('threads')->insert([
            'project_id' => $project_id,
            'created_at' => Helper::datetimeStamp(),
        ]);

        if ($request->file('projectimage')) {
            $file = $request->file('projectimage');
            $name = $file->getClientOriginalName();
            $path = '/users/' . md5($id) . '/projects/';
            $newFileName = $this->getFileName($path, $name);
            $file->move(public_path($path), $newFileName);
            DB::table('projects')->where('id', $project_id)->update([
                'image' => $path . trim($newFileName),
            ]);
        }

        foreach ($request->project_invites as $pi) {
            DB::table('project_invites')->insert([
                'to_email' => $pi,
                'project_id' => $project_id,
                'timestamp' => Helper::datetimeStamp(),
            ]);
        }

        Session::flash('message', 'Project is Added');
        return redirect()->back();
    }

    public function getFileName($path, $fileName)
    {
        if (file_exists(public_path($path . $fileName))) {
            // Split filename into parts
            $pathInfo = pathinfo($fileName);
            $extension = isset($pathInfo['extension']) ? ('.' . $pathInfo['extension']) : '';

            // Look for a number before the extension; add one if there isn't already
            if (preg_match('/(.*?)(\d+)$/', $pathInfo['filename'], $match)) {
                // Have a number; increment it
                $base = $match[1];
                $number = intVal($match[2]);
            } else {
                // No number; add one
                $base = $pathInfo['filename'];
                $number = 0;
            }
            // Choose a name with an incremented number until a file with that name
            // doesn't exist
            do {
                $fileName = $pathInfo['dirname'] . DIRECTORY_SEPARATOR . $base . '_' . ++$number . $extension;
            } while (file_exists(public_path($path . $fileName)));
        }

        return $fileName;
    }

}
