<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\MeetupParticipant;
use Hash;

class Log extends Model
{

    protected $table = 'logs';
 
    protected $fillable = [	
        'payload',
    ];

    protected $casts = [
        'payload' => 'array'
    ];    
    
}
