<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\MeetupParticipant;
use Hash;

class Meetup extends Model
{

    protected $table = 'meetup';
 
    protected $fillable = [	
        'name',
        'password',
        'status',
        'is_conferenc',
    ];
    
    protected $hidden = [
        'password',
    ]; 
       
    public function participants(){
        return $this->hasMany(MeetupParticipant::class, 'meetup_id');
    }

    public static function generate($id){
        return [
            'name' => Hash::make(config('app.name').'-'.$id.'-'.microtime()),
            'password' => Hash::make(config('app.name').'-'.$id.'-password'),
        ];
    }
    
}
