<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeetupParticipant extends Model
{

    protected $table = 'meetup_participants';
 
    protected $fillable = [	
        'meetup_id',
        'participant_id',
        'owner',
    ];

    
}
