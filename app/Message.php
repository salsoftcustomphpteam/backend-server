<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{

    protected $table = 'messages';

    public $timestamps = false;        
 
    protected $fillable = [	
        'fromid',
        'toid',
        'content',
        'read',
        'call',
        'completed',
        'file',
        'thumb',
        'type',
        'created_at',
        'group_id',
        'updated_at'
    ];


}
