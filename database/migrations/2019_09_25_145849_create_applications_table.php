<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateApplicationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('applications', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('appid', 65535);
			$table->text('appkey', 65535);
			$table->text('secret_key', 65535)->nullable();
			$table->text('domain', 65535);
			$table->timestamps();
			$table->string('name', 191)->nullable();
			$table->text('description', 65535)->nullable();
			$table->string('user_id', 191);
			$table->string('slug', 191);
			$table->boolean('status')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('applications');
	}

}
