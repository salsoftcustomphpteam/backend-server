<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateChatUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('chat_users', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->text('appid', 65535);
			$table->string('uid', 191)->index('uid');
			$table->string('name', 191);
			$table->text('image', 65535)->nullable();
			$table->string('socket_id', 191)->nullable();
			$table->string('user_type', 191)->nullable();
			$table->boolean('online')->default(0);
			$table->timestamps();
			$table->boolean('deleted')->default(0);
			$table->boolean('isVideoIncluded')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('chat_users');
	}

}
