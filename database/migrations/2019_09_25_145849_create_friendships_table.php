<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFriendshipsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('friendships', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('sender_type');
			$table->bigInteger('sender_id')->unsigned();
			$table->string('recipient_type');
			$table->bigInteger('recipient_id')->unsigned();
			$table->boolean('status')->default(0);
			$table->timestamps();
			$table->index(['sender_type','sender_id']);
			$table->index(['recipient_type','recipient_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('friendships');
	}

}
