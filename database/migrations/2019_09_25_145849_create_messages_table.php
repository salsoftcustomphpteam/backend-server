<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMessagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('messages', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('fromid', 191);
			$table->string('toid', 191)->nullable();
			$table->text('content', 65535)->nullable();
			$table->string('created_at', 191)->nullable();
			$table->boolean('read')->default(0);
			$table->boolean('call')->default(0);
			$table->string('completed', 120)->nullable();
			$table->text('file', 65535)->nullable();
			$table->text('thumb', 65535)->nullable();
			$table->string('type', 191)->nullable();
			$table->string('group_id', 191)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('messages');
	}

}
