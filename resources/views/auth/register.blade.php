@extends('layouts.app')
@section('title','Register')
@section('body-class','bg-full-screen-image  pace-done')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <section class="flexbox-container">
                    <div class="col-12 d-flex align-items-center justify-content-center">
                        <div class="col-lg-4 col-md-10 col-10 box-shadow-2 p-0">
                            <div class="card rad border-grey border-lighten-3 px-1 py-1 m-0">
                                <div class="card-header border-0">
                                <div class="card-title text-center">
                                  <img src="https://v4.soichat.com/assets/img/soighost_192x192.png" class="img-fluid" alt="branding logo">
                                    SOACHAT
                                </div>

                                </div>
                                <div class="card-content logn-form">

                                    <div class="card-body">
                                        @if ($errors->has('name'))
                                            <div class="alert alert-danger" role="alert">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </div>
                                        @elseif ($errors->has('email'))
                                            <div class="alert alert-danger" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </div>
                                        @elseif ($errors->has('password'))
                                            <div class="alert alert-danger" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </div>
                                        @endif

                                        <form class="form-horizontal" action="{{ route('register') }}" method="post">
                                            @csrf
                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input type="text" class="form-control"
                                                       class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}"
                                                       name="name" value="{{ old('name') }}" placeholder="User Name"
                                                       required>
                                                <div class="form-control-position">
                                                    <i class="fa fa-user-circle"></i>
                                                </div>
                                            </fieldset>

                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input type="text"
                                                       class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                                       name="email" value="{{ old('email') }}"
                                                       placeholder="Email Address"
                                                       required>
                                                <div class="form-control-position">
                                                    <i class="fa fa-envelope"></i>
                                                </div>
                                            </fieldset>

                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input type="password"
                                                       class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                       name="password" placeholder="Password"
                                                       required>
                                                <div class="form-control-position">
                                                    <i class="fa fa-key"></i>
                                                </div>
                                            </fieldset>

                                            <fieldset class="form-group position-relative has-icon-left">
                                                <input type="password" class="form-control" id="user-password"
                                                       name="password_confirmation" placeholder="Retype Password"
                                                       required>
                                                <div class="form-control-position">
                                                    <i class="fa fa-key"></i>
                                                </div>
                                            </fieldset>

                                            <button type="submit" class="btn btn-outline-primary btn-block"> Register
                                            </button>
                                        </form>
                                    </div>
                                    <p class="card-subtitle line-on-side text-muted text-center font-small-3 mx-2">
                                        <span>Already a User ? </span>
                                    </p>
                                    <div class="card-body">
                                        <a href="{{route('login')}}" class="btn btn-outline-danger btn-block"> Login</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

@endsection
