 <div class="main-menu menu-fixed menu-light menu-accordion" data-scroll-to-active="true">
        <div class="main-menu-content ps-container ps-theme-dark ps-active-y" data-ps-id="d67116d8-f431-3fd7-558c-6370ee832809" style="height: 274px;">

            <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                <li class="nav-item">
                    <a href="{{ url('documentation')  }}">
                        <i class="fa fa-user"></i>
                        <span class="menu-title" data-i18n="">Documentation</span>
                    </a>
                </li> 
                <li class="nav-item">
                    <a href="{{ url('create-application')  }}">
                        <i class="fa fa-user"></i>
                        <span class="menu-title" data-i18n="">Create Application</span>
                    </a>
                </li> 
                <li class="nav-item">
                    <a href="{{ url('my-application')  }}">
                        <i class="fa fa-user"></i>
                        <span class="menu-title" data-i18n="">My Application</span>
                    </a>
                </li> 
            </ul>
            <div class="ps-scrollbar-x-rail" style="left: 0px; bottom: 3px;">
                <div class="ps-scrollbar-x" tabindex="0" style="left: 0px; width: 0px;"></div>
            </div>
            <div class="ps-scrollbar-y-rail" style="top: 0px; right: 3px; height: 274px;">
                <div class="ps-scrollbar-y" tabindex="0" style="top: 0px; height: 258px;"></div>
            </div>
        </div>
    </div>
