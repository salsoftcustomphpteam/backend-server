@extends('layouts.app')
@section('title','Create Project')
@section('body-class','vertical-layout vertical-menu 2-columns fixed-navbar pace-done menu-expanded')
@section('content')
@section('css')
    <link rel="stylesheet" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/vendors/css/forms/selects/select2.min.css">
    <style type="text/css">
        .red{
            color: red;
        }
    </style>
@endsection

    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <strong>{{ Session::get('message')  }}</strong>
                    </div>
                @endif
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <strong>{{ Session::get('error')  }}</strong>
                    </div>
                @endif
                    <section id="configuration">
                        <div class="row">
                            <div class="col-12">
                                <div class="card rounded">
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard client-pro-main">
                                            <h1>Create your first Application</h1>
                                            <form action="{{url('application/create-application')}}" 
                                            method="post">
                                                @csrf
                                                <div class="">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="">Application Name</label>
                                                                <div class="position-relative has-icon-left">
                                                                    <input type="text" id="appname" class="form-control" name="name">
                                                                    <div class="form-control-position"> <i class="fa fa-user"></i> </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="">Application Domain</label>
                                                                <span class="red"></span>
                                                                <div class="position-relative has-icon-left">
                                                                    <input type="text" id="domain" class="form-control" name="domain">
                                                                    <div class="form-control-position"> <i class="fa fa-user"></i> </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="">Application Description</label>
                                                                <div class="position-relative has-icon-left">
                                                                    <textarea class="form-control" rows="5" name="description" id="editor"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="">User Types (comma separated)</label>
                                                                <div class="position-relative has-icon-left">
                                                                    <input class="form-control" rows="5" name="user_types"></input>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="">Events (comma separated)</label>
                                                                <div class="position-relative has-icon-left">
                                                                    <input class="form-control" rows="5" name="events"></input>
                                                                </div>
                                                            </div>
                                                        </div>


                                                    </div>

                                                    <div class="cntr-btnn-main">
                                                        <button type="submit">Save Changes</button>
                                                    </div>
                                                </div>
                                                <!--admin add custmr end-->
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
            </div>
        </div>
    </div>

@endsection
@section('js')
<script type="text/javascript">
    function CheckIsValidDomain(domain) { 
        var re = new RegExp(/^((?:(?:(?:\w[\.\-\+]?)*)\w)+)((?:(?:(?:\w[\.\-\+]?){0,62})\w)+)\.(\w{2,6})$/); 
        return domain.match(re);
    } 

    $("#domain").on('change', function(){
        if(!CheckIsValidDomain($(this).val())){            
            $(this).val('');
            $('.red').html('Invalid Domain');
        }
        else{
            $('.red').html('');

        }
    })
</script>
@endsection
