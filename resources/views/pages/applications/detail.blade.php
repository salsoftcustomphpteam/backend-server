@extends('layouts.app')
@section('title','Application Details')
@section('body-class','vertical-layout vertical-menu 2-columns fixed-navbar pace-done menu-expanded')
@section('content')
@section('css')
    <link rel="stylesheet" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/vendors/css/forms/selects/select2.min.css">
@endsection
<div class="app-content content">
<div class="content-wrapper">
   <div class="content-body">
      <section id="combination-charts">
         <div class="row">
            <div class="col-12">
               <div class="card">
                  <div class="card-content">
                     <div class="card-body">
                        <div class="text-center">
                           <h3>Application Details</h3>
                        </div>
                        <ul class="nav nav-tabs nav-topline">
                           <li class="nav-item">
                              <a class="tabclicker nav-link active" data-id="0" id="base-tab21" data-toggle="tab" aria-controls="tab21"
                                 href="#privacysettings" aria-expanded="true">Privacy Settings</a>
                           </li>
                           <li class="nav-item">
                              <a class="tabclicker nav-link" data-id="1" id="base-tab22" data-toggle="tab" aria-controls="tab22"
                                 href="#userinfo" aria-expanded="false">User Information</a>
                           </li>
                           <li class="nav-item">
                              <a class="tabclicker nav-link" data-id="1" id="base-tab22" data-toggle="tab" aria-controls="tab24"
                                 href="#events" aria-expanded="false">Events</a>
                           </li>
                           <li class="nav-item">
                              <a class="tabclicker nav-link" data-id="1" id="base-tab22" data-toggle="tab" aria-controls="tab25"
                                 href="#channels" aria-expanded="false">Channels</a>
                           </li>
                           <li class="nav-item">
                              <a class="tabclicker nav-link" data-id="2" id="base-tab23" data-toggle="tab" aria-controls="tab23"
                                 href="#messages" aria-expanded="false">Messages</a>
                           </li>
                           <li class="nav-item">
                              <a class="tabclicker nav-link" data-id="2" id="base-tab23" data-toggle="tab" aria-controls="tab23"
                                 href="#Logs" aria-expanded="false">Logs</a>
                           </li>

                        </ul>
                        <div class="tab-content px-1 pt-1 border-grey border-lighten-2 border-0-top">
                           <div role="tabpanel" class="tab-pane active" id="privacysettings" aria-expanded="true" aria-labelledby="base-tab31">
                              <div class="row">
                                  <div class="col-md-12">
                                      <div class="form-group">
                                          <label>App Id</label>
                                          <input type="text" class="form-control" disabled value="{{$rec->appid}}">
                                      </div>
                                      <div class="form-group">
                                          <label>App Key</label>
                                          <input type="text" class="form-control" disabled value="{{$rec->appkey}}">
                                      </div>
                                      <div class="form-group">
                                          <label>Secret Key</label>
                                          <input type="text" class="form-control" disabled value="{{$rec->secret_key}}">
                                      </div>
                                      <div class="form-group">
                                          <label>Service Name</label>
                                          <input type="text" class="form-control" disabled value="{{$rec->slug}}">
                                      </div>
                                      <div class="form-group">
                                          <label>App Name</label>
                                          <input type="text" class="form-control" disabled value="{{$rec->name}}">
                                      </div>
                                      <div class="form-group">
                                          <label>App Domain</label>
                                          <input type="text" class="form-control" disabled value="{{$rec->domain}}">
                                      </div>
                                      <div class="form-group">
                                          <label>App Description</label>
                                           <textarea disabled class="form-control">{{$rec->description}}</textarea>
                                      </div>
                                  </div>
                              </div>
                           </div>
                           <div class="tab-pane" id="userinfo" aria-labelledby="base-tab32">
                              <h4>Users</h4>
                              <div class="card-content">
                                <div class="card-content">
                                    <div class="card-body  py-0 px-0">
                                        <div class="list-group">
                                            @foreach($rec->users as $u)
                                             @php
                                                if($u->online)
                                                    $status = 'avatar-online';
                                                else
                                                    $status = 'avatar-busy';
                                            @endphp
                                            <a href="javascript:void(0)" class="list-group-item">
                                                <div class="media">
                                                    <div class="media-left pr-1">
                                                        <span class="avatar avatar-sm {{$status}} rounded-circle">
                                                        <img src="{{$u->image}}" alt="avatar"><i></i></span></div>
                                                    <div class="media-body w-100">
                                                        <h6 class="media-heading mb-0">{{$u->name}}</h6>
                                                        <p class="font-small-2 mb-0 text-muted">User ID: {{$u->uid}}</p>
                                                    </div>
                                                </div>
                                            </a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>  
                           </div>
                           <div class="tab-pane" id="messages" aria-labelledby="base-tab33">
                              sdvsdvsdv
                           </div>
                           <div class="tab-pane" id="events" aria-labelledby="base-tab34">
                           <h4>Events</h4>
                              <div class="card-content">
                                <div class="card-content">
                                    <div class="card-body  py-0 px-0">
                                        <div class="list-group">
                                            @foreach($rec->events as $event)
                                                <input type="readonly" value="{{$event->name}}" class="form-control">
                                            @endforeach
                                        </div>
                                    </div>
                                </div>                           
                            </div>
                            </div>
                           <div class="tab-pane" id="channels" aria-labelledby="base-tab35">
                           <h4>Channels</h4>
                              <div class="card-content">
                                <div class="card-content">
                                    <div class="card-body  py-0 px-0">
                                        <div class="list-group">
                                            @foreach($rec->channels as $channel)
                                                <input type="readonly" value="{{$channel->name}}" class="form-control">
                                            @endforeach
                                        </div>
                                    </div>
                                </div>                            
                            </div>
                            </div>
                            <div class="tab-pane" id="logs" aria-labelledby="base-tab35">
                            <h4>logs</h4>
                              <div class="card-content">
                                <div class="card-content">
                                    <div class="card-body  py-0 px-0">
                                        <div class="list-group">
                                        </div>
                                    </div>
                                </div>                            
                            </div>
                            </div>

                        </div>
                     </div>
                  </div>
               </div>
            </div>
      </section>
      </div>
   </div>
</div>

@endsection
@section('js')

@endsection
