@extends('layouts.app')
@section('title','Create Project')
@section('body-class','vertical-layout vertical-menu 2-columns fixed-navbar pace-done menu-expanded')
@section('content')
@section('css')
    <link rel="stylesheet" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/vendors/css/forms/selects/select2.min.css">
@endsection

    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <strong>{{ Session::get('message')  }}</strong>
                    </div>
                @endif
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <strong>{{ Session::get('error')  }}</strong>
                    </div>
                @endif
                    <section id="configuration">
                        <div class="row">
                            <div class="col-12">
                                <div class="card rounded">
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard client-pro-main">
                                            <h1>Create your first Application</h1>
                                            <form action="{{url('application/update-application')}}" 
                                            method="post">
                                                @csrf
                                                <input type="hidden" name="form_token_" value="{{Crypt::encryptString($rec->id)}}">
                                                <div class="">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="timesheetinput1">App Id</label>
                                                                <div class="position-relative has-icon-left">
                                                                    <input value="{{$rec->appid}}" type="text" class="form-control" disabled>
                                                                    <div class="form-control-position"> <i class="fa fa-user"></i> </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="timesheetinput1">App Key</label>
                                                                <div class="position-relative has-icon-left">
                                                                    <input value="{{$rec->appkey}}" type="text" class="form-control" disabled>
                                                                    <div class="form-control-position"> <i class="fa fa-user"></i> </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="timesheetinput1">Application Name</label>
                                                                <div class="position-relative has-icon-left">
                                                                    <input value="{{$rec->name}}" type="text" id="timesheetinput1" class="form-control" name="name">
                                                                    <div class="form-control-position"> <i class="fa fa-user"></i> </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="timesheetinput1">Application Domain</label>
                                                                <div class="position-relative has-icon-left">
                                                                    <input value="{{$rec->domain}}" type="text" id="timesheetinput1" class="form-control" name="domain">
                                                                    <div class="form-control-position"> <i class="fa fa-user"></i> </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="timesheetinput1">Application Description</label>
                                                                <div class="position-relative has-icon-left">
                                                                    <textarea class="form-control" rows="5" name="description" id="editor">{{$rec->description}}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="cntr-btnn-main">
                                                        <button type="submit">Save Changes</button>
                                                    </div>
                                                </div>
                                                <!--admin add custmr end-->
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script src="https://cdn.ckeditor.com/4.11.0/standard/ckeditor.js"></script>
    <script src="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script type="text/javascript">
        $(".select2-tags").select2({
            tags: !0
        });
        // CKEDITOR.replace( 'editor' );
        // function readURL(input, target) {
        //     if (input.files && input.files[0]) {
        //         var reader = new FileReader();
        //         reader.onload = function(e) {
        //             $('#'+target).attr('src', e.target.result);
        //         }
        //         reader.readAsDataURL(input.files[0]);
        //     }
        // }

    </script>
@endsection
