@extends('layouts.app')
@section('title','My Projects')
@section('body-class','vertical-layout vertical-menu 2-columns fixed-navbar pace-done menu-expanded')
@section('content')
    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-body">

                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <strong>{{ Session::get('message')  }}</strong>
                    </div>
                @endif
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <strong>{{ Session::get('error')  }}</strong>
                    </div>
              @endif


            <!-- Basic form layout section start -->
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card rounded">
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">

                                        <h1 class="pull-left">My Application</h1>
                                        <a href="{{url('/create-application')}}" class="green-btn-project"><i class="fa fa-plus-circle"></i> Add Applciations</a>

                                        <div class="maain-tabble">
                                            <table class="table table-striped table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    <th>App id</th>
                                                    <th>App key</th>
                                                    <th>Domain</th>
                                                    <th>Created at</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach ($rec as $key => $j)
                                                    <tr id="row{{$j->id}}">
                                                        <td>{{$j->id}}</td>
                                                        <td>{{$j->name}}</td>
                                                        <td>{{$j->appid}}</td>
                                                        <td>{{$j->appkey}}</td>
                                                        <td>{{$j->domain}}</td>
                                                        <td>{{$j->created_at}}</td>
                                                        @if($j->status==0)
                                                            <td><label class="badge badge-success">Active</label></td>
                                                        @else
                                                        <td><label class="badge badge-danger">InActive</label></td>
                                                        @endif
                                                        <td>
                                                            <div class="btn-group mr-1 mb-1">
                                                                <button type="button" class="btn dropdown-toggle btn-drop-table btn-sm" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></button>
                                                                <div class="dropdown-menu" x-placement="bottom-start" style="position: absolute; transform: translate3d(0px, 21px, 0px); top: 0px; left: 0px; will-change: transform;">
                                                                    <a class="dropdown-item" href="{{url('/application/detail/'.Crypt::encryptString($j->id))}}"><i class="fa fa-pencil-square-o"></i>details</a>
                                                                    <a class="dropdown-item" href="{{url('/application/edit/'.Crypt::encryptString($j->id))}}"><i class="fa fa-pencil-square-o"></i>Edit</a>
                                                                    <a class="dropdown-item" href="{{url('/application/update-status/'.Crypt::encryptString($j->id))}}"><i class="fa fa-times"></i>Active/Inactive</a>
                                                                </div>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                    </div><!--card body end-->
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>
    <div id="userInvites" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">User Invites</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <ul id="appendData"></ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('js')
    <script type="text/javascript">
        $(".viewinvites").on('click', function () {
            var id = $(this).data("id");
            $.ajax({
                url: base_url + '/project/get-project-user-invites/'+id,
                success: function (response) {
                    $("#appendData").html(response);
                }
            })
        })
    </script>
@endsection
