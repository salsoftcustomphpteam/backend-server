@extends('layouts.app')
@section('title','Create Project')
@section('body-class','vertical-layout vertical-menu 2-columns fixed-navbar pace-done menu-expanded')
@section('content')
@section('css')
    <link rel="stylesheet" href="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/vendors/css/forms/selects/select2.min.css">
@endsection

    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                @if(Session::has('message'))
                    <div class="alert alert-success">
                        <strong>{{ Session::get('message')  }}</strong>
                    </div>
                @endif
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <strong>{{ Session::get('error')  }}</strong>
                    </div>
            @endif
                    <section id="configuration">
                        <div class="row">
                            <div class="col-12">
                                <div class="card rounded">
                                    <div class="card-content collapse show">
                                        <div class="card-body card-dashboard client-pro-main">
                                            <h1>Create Project</h1>
                                            <form action="{{url('projects/submit-project')}}" enctype="multipart/form-data" method="post">
                                                @csrf
                                                <div class="">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="timesheetinput1">Project Title</label>
                                                                <div class="position-relative has-icon-left">
                                                                    <input type="text" id="timesheetinput1" class="form-control" name="title">
                                                                    <div class="form-control-position"> <i class="fa fa-user"></i> </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <fieldset class="form-group">
                                                                <label for="basicInputFile">Project Image</label>
                                                                <div class="custom-file">
                                                                    <input type="file" class="custom-file-input" name="projectimage" id="projectimage">
                                                                    <label class="custom-file-label" for="projectimage">Choose file</label>
                                                                </div>
                                                            </fieldset>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label>Project Status:</label>
                                                                <select class="form-control" name="status">
                                                                    <option value="Public">Public</option>
                                                                    <option value="Private">Private</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label>Invite User:</label>
                                                                <select placeholder="Invite Users.." name="project_invites[]" class="select2-tags form-control" multiple="" id="select2-tags">
                                                                    @php
                                                                        $users = DB::table('users')->where('type',0)->where('id','!=',Auth::user()->id)->get();
                                                                    @endphp
                                                                    @foreach($users as $u)
                                                                        <option value="{{$u->email}}">{{$u->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="timesheetinput1">Job Description</label>
                                                                <div class="position-relative has-icon-left">
                                                                    <textarea name="description" id="editor"></textarea>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>

                                                    <div class="cntr-btnn-main">
                                                        <button type="submit">Save Changes</button>
                                                    </div>
                                                </div>
                                                <!--admin add custmr end-->
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
            </div>
        </div>
    </div>

@endsection
@section('js')
    <script src="https://cdn.ckeditor.com/4.11.0/standard/ckeditor.js"></script>
    <script src="https://pixinvent.com/stack-responsive-bootstrap-4-admin-template/app-assets/vendors/js/forms/select/select2.full.min.js"></script>
    <script type="text/javascript">
        $(".select2-tags").select2({
            tags: !0
        });
        document.getElementById('projectimage').onchange = uploadOnChange;
        function uploadOnChange() {
            var filename = this.value;
            var lastIndex = filename.lastIndexOf("\\");
            if (lastIndex >= 0) {
                filename = filename.substring(lastIndex + 1);
            }
            $('.custom-file-label').text(filename);
        }
        CKEDITOR.replace( 'editor' );
        function readURL(input, target) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#'+target).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>
@endsection
