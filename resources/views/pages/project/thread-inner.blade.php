@extends('layouts.app')
@section('title','Discussion Thread')
@section('body-class', "vertical-layout vertical-menu 2-columns menu-expanded fixed-navbar")
@section('body-col', "2-column")
@section('content')


    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-detached content-left">
                <div class="content-body">
                    <section class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-head">
                                    <div class="card-header">
                                        <h4 class="card-title">Thread Discussion</h4>
                                    </div>
                                </div>

                                <div class="card-body">
                                    <div id="threadMsg"
                                         style="background:white; overflow-y: scroll; overflow-x: hidden;  height:400px;">
                                    </div>
                                    <form id="threadForm" enctype="multipart/form-data" method="post">
                                        <input type="hidden" name="thread_id" value="" id="thread_id">
                                        <div class="disucs-comment-sec clearfix">
                                            <label><i class="fa fa-commenting"></i> Enter your query
                                                Here</label>
                                            <div class="disucs-comment-sec-txtara clearfix">
                                                <textarea name="msg" id="msg"></textarea>
                                                <button type="button" name="file" class="up-doc"
                                                        onclick="document.getElementById('upload').click()">
                                                    Attach: &nbsp;<i class="fa fa-paperclip"></i></button>
                                                <input type="file" id="upload" name="attachment">
                                                <span id="filename"></span>
                                            </div>
                                            <button type="button" class="send btn btn-primary">Send</button>

                                    </form>

                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <!-- // Basic form layout section end -->
            <div class="sidebar-detached sidebar-right" ,=",">
                <div class="sidebar"><div class="project-sidebar-content">

                        <div class="card">
                            <div class="card-header mb-0">
                                <h5 class="card-title">Project Users</h5>
                                <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                            </div>
                            <div class="card-content">
                                <div class="card-content">
                                    <div class="card-body  py-0 px-0">
                                        <div class="list-group">
                                            <a href="javascript:void(0)" class="list-group-item">
                                                <div class="media">
                                                    <div class="media-left pr-1"><span class="avatar avatar-sm avatar-online rounded-circle"><img src="../../../app-assets/images/portrait/small/avatar-s-1.png" alt="avatar"><i></i></span></div>
                                                    <div class="media-body w-100">
                                                        <h6 class="media-heading mb-0">Margaret Govan</h6>
                                                        <p class="font-small-2 mb-0 text-muted">Project Owner</p>
                                                    </div>
                                                </div>
                                            </a>
                                            <a href="javascript:void(0)" class="list-group-item">
                                                <div class="media">
                                                    <div class="media-left pr-1"><span class="avatar avatar-sm avatar-busy rounded-circle"><img src="../../../app-assets/images/portrait/small/avatar-s-2.png" alt="avatar"><i></i></span></div>
                                                    <div class="media-body w-100">
                                                        <h6 class="media-heading mb-0">Bret Lezama</h6>
                                                        <p class="font-small-2 mb-0 text-muted">Project Manager</p>
                                                    </div>
                                                </div>
                                            </a>
                                            <a href="javascript:void(0)" class="list-group-item">
                                                <div class="media">
                                                    <div class="media-left pr-1"><span class="avatar avatar-sm avatar-online rounded-circle"><img src="../../../app-assets/images/portrait/small/avatar-s-3.png" alt="avatar"><i></i></span></div>
                                                    <div class="media-body w-100">
                                                        <h6 class="media-heading mb-0">Carie Berra</h6>
                                                        <p class="font-small-2 mb-0 text-muted">Senior Developer</p>
                                                    </div>
                                                </div>
                                            </a>
                                            <a href="javascript:void(0)" class="list-group-item">
                                                <div class="media">
                                                    <div class="media-left pr-1"><span class="avatar avatar-sm avatar-away rounded-circle"><img src="../../../app-assets/images/portrait/small/avatar-s-6.png" alt="avatar"><i></i></span></div>
                                                    <div class="media-body w-100">
                                                        <h6 class="media-heading mb-0">Eric Alsobrook</h6>
                                                        <p class="font-small-2 mb-0 text-muted">UI Developer</p>
                                                    </div>
                                                </div>
                                            </a>
                                            <a href="javascript:void(0)" class="list-group-item">
                                                <div class="media">
                                                    <div class="media-left pr-1"><span class="avatar avatar-sm avatar-busy rounded-circle"><img src="../../../app-assets/images/portrait/small/avatar-s-7.png" alt="avatar"><i></i></span></div>
                                                    <div class="media-body w-100">
                                                        <h6 class="media-heading mb-0">Berra Eric</h6>
                                                        <p class="font-small-2 mb-0 text-muted">UI Developer</p>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ Project Users -->
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>


@endsection
@section('js')


@endsection
