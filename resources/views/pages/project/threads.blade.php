@extends('layouts.app')
@section('title','Discussion Thread')
@section('body-class', "vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col', "2-column")
@section('content')

    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-body">
                <section id="configuration">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Discussion Thread</h4>
                                    <a class="heading-elements-toggle"><i class="fa fa-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard table-responsive">
                                        <table class="table  table-striped table-bordered zero-configuration">
                                            <thead>
                                            <tr>
                                                <th>Order #</th>
                                                <th>Title</th>
                                                <th>Created At</th>
                                                <th>Replies</th>
                                                <th>View</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($threads as $t)
                                                <tr>
                                                    <td>{{$t->project_id}}</td>
                                                    <td>{{$t->title}}</td>
                                                    <td>{{date('Y-m-d',strtotime($t->created_at))}}</td>
                                                    <td>0</td>
                                                    <td>
                                                        <a href="{{url('/project/threads/conversation/'.$t->id)}}"> <b>View</b></a>
                                                    </td>
                                                </tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>


@endsection
@section('js')
@endsection
