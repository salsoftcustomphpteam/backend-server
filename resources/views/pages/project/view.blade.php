@extends('layouts.app')
@section('title','Project Details')
@section('body-class', "vertical-layout vertical-menu 2-columns   menu-expanded fixed-navbar")
@section('body-col', "2-column")
@section('content')

    <div class="app-content content">
        <div class="content-wrapper">

            <div class="content-body">
                <!-- Basic form layout section start -->
                <section id="basic-form-layouts">

                    <div class="card pro-main">
                        <div class="row">
                            <div class="col-lg-6 col-12">
                                <div class="profile-frm">
                                    <h1>Project Details</h1>
                                    <div class="row">
                                        <div class="col-lg-4 col-12">
                                            <div class="pro-img-main">
                                                <h3>Image</h3>
                                                @if($project->image == '')
                                                    <img id="user_ava"
                                                         src="https://cdn1.vectorstock.com/i/1000x1000/77/30/default-avatar-profile-icon-grey-photo-placeholder-vector-17317730.jpg"
                                                         alt=""/>
                                                @else
                                                    <img id="user_ava" src="{{asset($project->image)}}" alt=""/>
                                                @endif
                                            </div>

                                        </div><!--col end-->

                                        <div class="col-lg-8 col-12 mt-1">
                                            <div class="">
                                                <h3>Title</h3>
                                                <p>{{$project->title}} <br>
                                                <small>Created At: {{$project->created_at}}</small></p>
                                            </div><!--pro inner row end-->

                                            <div class="">
                                                <h3>Status</h3>
                                                <p>{{$project->status}}</p>
                                            </div><!--pro inner row end-->

                                            <div class="">
                                                <h3>User Invites</h3>
                                                <ul>
                                                    @php
                                                        $inv = DB::table('project_invites')->where('project_id',$project->id)->get();
                                                        foreach ($inv as $i){
                                                        $user = \App\Helper::getUsersInfoByEmail($i->to_email);
                                                         if($user)
                                                             echo '<li class="text-primary"><i class="fa fa-dot-circle-o">&nbsp;&nbsp;</i> '.$user->name.'</li>';
                                                         else
                                                            echo '<li class="text-danger"><i class="fa fa-dot-circle-o">&nbsp;&nbsp;</i> '.$i->to_email.'</li>';
                                                        }
                                                    @endphp
                                                </ul>
                                            </div><!--pro inner row end-->

                                            <div class="">
                                                <h3>Description</h3>
                                                {!! $project->description !!}
                                            </div><!--pro inner row end-->
                                        </div>

                                    </div><!--row end-->


                                </div><!--profile form end-->

                            </div><!--col left end-->
                        </div>

                    </div><!--card end-->

                </section>
                <!-- // Basic form layout section end -->
            </div>
        </div>
    </div>

@endsection
@section('js')

    <script type="text/javascript">

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#user_ava').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#upload").change(function () {
            readURL(this);
        });

    </script>

@endsection
