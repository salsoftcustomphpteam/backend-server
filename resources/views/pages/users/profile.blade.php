@extends('layouts.app')
@section('title','Profile')
@section('body-class','vertical-layout vertical-menu 2-columns fixed-navbar pace-done menu-expanded')
@section('content')

<div class="app-content content">
    <div class="content-wrapper">

        <div class="content-body">
            @if(Session::has('message'))
                <div class="alert alert-success">
                    <strong>{{ Session::get('message')  }}</strong>
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger">
                    <strong>{{ Session::get('error')  }}</strong>
                </div>
        @endif

        <!-- Basic form layout section start -->
            <section id="basic-form-layouts">
                <div class="row match-height">
                    <div class="col-xl-12 col-lg-12 col-12">
                        <div class="profile-frm card admin-formmm">


                            <h1><i class="fa fa-user"></i>Profile Settings</h1>
                            <div class="clearfix"></div>
                            <form action="{{ url('/update-profile')  }}"
                                  enctype="multipart/form-data" method="post" novalidate class="form">
                                @csrf
                                <input type="hidden" name="token_id" value="{{ Crypt::encryptString($user->id) }}">
                                <div class="row">
                                    <div class="col-md-2 col-sm-12">
                                        <button type="button" style="margin-top: 0;" name="file" class="uplon-btn" onclick="document.getElementById('upload').click()">
                                            <img src="{{ ($user->image) ? asset($user->image) : asset('/images/upload-img.jpg')}}" id="personal_img" class="img-full" alt=""></button>
                                        <input type="file"  accept="image/*" required onchange="readURL(this, 'personal_img')"  id="upload" name="personal_img">
                                </div>
                                    <div class="col-md-10 col-sm-12">
                                        <div class="form-body">

                                            <div class="row">
                                                <div class="col-md-4 col-sm-12">
                                                    <div class="form-group">
                                                        <label>Name:</label>
                                                        <input type="text" value="{{$user->name}}" id="name" class="form-control"
                                                               placeholder="" name="name">
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-12">
                                                    <div class="form-group">
                                                        <label>Email:</label>
                                                        <input type="email" disabled value="{{$user->email}}" id="email"
                                                               class="form-control" placeholder="" name="email">
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-12">
                                                    <div class="form-group">
                                                        <label>Gender:</label>
                                                        <select class="form-control" name="gender">
                                                            <option {{($user->gender == 'Male') ? "selected" : ""}} value="Male">Male</option>
                                                            <option {{($user->gender == 'Female') ? "selected" : ""}} value="Female">Female</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 col-sm-12">
                                                    <div class="form-group">
                                                        <label>Address:</label>
                                                        <input type="text" value="{{ $user->address }}" id="autocomplete" onfocus="geolocate()"
                                                               class="form-control" placeholder="Enter your address"
                                                               name="address">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-3 col-sm-12">
                                                    <div class="form-group">
                                                        <label>Country:</label>
                                                        <input type="text" value="{{$user->country}}" id="country" class="form-control"
                                                               placeholder="Enter your Country"
                                                               name="country">
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12">
                                                    <div class="form-group">
                                                        <label>State:</label>
                                                        <input type="text" value="{{$user->state}}" id="administrative_area_level_1" class="form-control"
                                                               placeholder="" name="state">
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12">
                                                    <div class="form-group">
                                                        <label>City:</label>
                                                        <input type="text" value="{{$user->city}}" id="locality" class="form-control"
                                                               placeholder="" name="city">
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12">
                                                    <div class="form-group">
                                                        <label>Zip Code:</label>
                                                        <input type="text" value="{{$user->zipcode}}" id="postal_code" class="form-control"
                                                               placeholder="" name="zip_code">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 col-sm-12">
                                                    <div class="form-group">
                                                        <label>Phone number:</label>
                                                        <input type="number" id="number" value="{{$user->telephone}}" class="form-control"
                                                               placeholder="" name="phone">
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-12">
                                                    <div class="form-group">
                                                        <label>Date of Birth:</label>
                                                        <input type="date" id="dob" value="{{$user->dob}}" class="form-control"
                                                               placeholder="" name="dob">
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-12">
                                                    <div class="form-group">
                                                        <label>Update Password or Leave Empty</label>
                                                        <input type="password" id="password" class="form-control" placeholder=""
                                                               name="password">
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-primary">
                                        Update Profile
                                    </button>
                                </div>
                            </form>

                        </div><!--profile form end-->

                    </div><!--col left end-->
                </div>

            </section>
        </div>
    </div>
</div>

@endsection
@section('js')
    <script type="text/javascript">
        function readURL(input, target) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#'+target).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>
@endsection
