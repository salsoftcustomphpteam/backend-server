<!DOCTYPE html>
<html lang="en">
<head>
  <title>SoaChat</title>
  <meta charset="utf-8">
  <meta "name"="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://highlightjs.org/static/demo/styles/atom-one-dark.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  <script src="https://highlightjs.org/static/highlight.pack.js"></script>
</head>
<body>
<style type="text/css">
  .icon{
    width: 30px;
    padding-right: 10px;
  }
</style>
<div class="header text-right">

</div>
<div class="jumbotron p-0">
  <div class="container text-right" style="float-right">
      <a class="pull-right p-1" href="{{url('login')}}"><b>Login</b></a>
      <a class="pull-right p-1" href="{{url('register')}}"><b>Register</b></a>
  </div>
  <div class="p-5 text-center">
      <h1>SOA CHAT with Feathers </h1>
      <p>Overall solution for realtime APIS, Events, messaging, video calling, chat bot and so much.</p>
      <p>Signup now to get exciting features of Soachat Feathers</p>
  </div>
</div>
  
<div class="container">
  <div class="row">

    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">Installation</div>
            <div class="card-body">
                <div class="common protocols">
                    <ul class="nav nav-tabs nav-topline">
                      <li class="nav-item">
                        <a class="nav-link active" id="base-tab21" data-toggle="tab" aria-controls="tab21" href="#tab21" role="tab" aria-selected="true"><img class="icon" src="https://laravel.com/img/favicon/favicon.ico">Laravel</a>
                      </li>
                    </ul>
          <div class="tab-content px-1 pt-1 border-grey border-lighten-2 border-0-top">
            <div class="tab-pane active" id="tab21" role="tab" aria-labelledby="base-tab21">
    
    <hr>
    <div class="col-sm-12 m-4">
    </div>

    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">Create a PHP wrapper soachat.php</div>
            <div class="card-body">
                <div class="common protocols">
                  <pre><code class="json">
                    
                    namespace App\Core\Soachat;

                    // namespace App;

                    class Soachat
                    {
                        /*  Declearing The static variables */
                        /*  Enter the your app id you obtained while registration */
                        /*  Enter the your secret_key you obtained while registration */
                        /*  Never show or reveal your secret_key */

                        static $app_id = 'e67b3a61e280c574b02cbc6f0a584fb8';
                        static $secret_key = '$2y$10$McZihofEO/ISx5mPtkPCneP0S.r30dkYWRjz6nhNoofppHLk6dlLK';
                        static $feathers_endpoint = 'http://dev71.onlinetestingserver.com:4000';
                        static $service_name = 'sentinelsos';

                        
                        /* A curl Helper to perform HTTP POST */
                        public static function request($fields, $url=''){
                            $data_string = json_encode($fields);
                            $ch = curl_init ();
                                
                            if ($ch === false) {
                                throw new Exception('failed to initialize');
                            }

                            curl_setopt ($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                            'Content-Type: application/json',
                                            'Content-Length: ' . strlen($data_string))
                            );
                            $response = curl_exec ( $ch );
                            return $response;
                            curl_close ( $ch );
                            return $response;
                        } 
                        
                      
                        /*  Endpoint to Emit Event From Soachat-Feather Server system POST - /{serviceName} */
                        public static function emitEvent($event, $channel, $payload=null){

                            $data = [
                                'event' => $event,
                                'channel' => $channel,
                                'payload' => $payload
                            ]; 

                            $url = self::$feathers_endpoint .'/'. self::$service_name;
                            $response = self::request($data, $url);
                            return $response;
                        }   
                      
                      
                    }   
                </code></pre>
                </div>
            </div> 

        </div>
    </div>
        
         
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">To Emit Events (SERVER)</div>
            <div class="card-body">
                <div class="common protocols">
                  <pre><code class="json">
                  //params
                  1) EVENT
                  2) USER
                  3) PAYLOAD 
                  Soachat::emitEvent('REGISTRATION','USER','Andy has registered');
                </code></pre>
                </div>
            </div> 

        </div>
    </div>
        
             
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">CONFIGURATION</div>
            <div class="card-body">
                <div class="common protocols">
                  <pre><code class="json">
                  
                    1) include SocketIO client Libraray 
                    https://cdn.jsdelivr.net/npm/socket.io-client@2/dist/socket.io.js

                    $(function(){
                      2) Connect To Soachat Feather Server
                      var socket = io('http://dev71.onlinetestingserver.com:4000');

                      
                      3) Join the Channel
                      socket.emit(
                      'create', 
                      'sentinelsos',{
                          event:  'JOIN_CHANNEL',
                          channel: 'ADMIN',
                          user: {
                              user_id: 'LOGIN ID', type: "ADMIN"
                          },
                          payload: {}          
                      });
                      
                      4) Listen the Events
                      socket.on('sentinelsos REGISTRATION', function(data){
                          console.log(data);
                      });


                      5) ENJOY
                      
                  });                    
                  
                </code></pre>
                </div>
            </div> 

        </div>
    </div>
        

  </div>
</div>
<script>
    hljs.initHighlightingOnLoad();
</script>  
</body>
</html>
