<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Setup CORS */
// header('Access-Control-Allow-Origin: *');
// header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method, Authorization");
// header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

$http_origin = @$_SERVER['HTTP_ORIGIN'];
if ($http_origin == "http://dev71.onlinetestingserver.com"  || 
    $http_origin == "https://dev71.onlinetestingserver.com" ||
    $http_origin == "http://ucollabs.com" ||
    $http_origin == "https://ucollabs.com"
)
{  
    header("Access-Control-Allow-Origin: $http_origin");
}

if (isset($_SERVER['HTTP_ORIGIN'])) {
    // should do a check here to match $_SERVER['HTTP_ORIGIN'] to a
    // whitelist of safe domains
    //header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
// Access-Control headers are received during OPTIONS requests
if (@$_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

}

Route::post('register', 'ApiController@register');
Route::post('loginForApi', 'ApiController@apiLogin');
Route::post('login', 'ApiController@login');
Route::post('/authenticate', 'ApiController@authenticate');
Route::post('/user/add', 'ApiController@addUser');
Route::post('get-applications','ApiController@getApps');
Route::post('saveLog','ApiController@saveLog');


Route::group(['middleware' => ['auth:chatuser']], function () {

    Route::get('profile', 'ApiController@profile');
    Route::get('logout', 'ApiController@logout');

    Route::group(['prefix' => 'user'], function(){
        
        
        Route::group(['prefix' => 'appplication'], function(){
            Route::post('/', 'ApiController@myApplications');
            Route::post('/{id}', 'ApiController@applicationDetails');
            Route::post('/create', 'ApiController@createApplication');
            Route::post('/update', 'ApiController@updateApplication');
            Route::post('/users/{id}', 'ApiController@applicationUsers');
        });

        Route::post('/profile', 'ApiController@profile');
        Route::post('/all', 'ApiController@allUser');
        Route::post('/all', 'ApiController@allUser');
        Route::post('/bulk-add', 'ApiController@addUsersBulk');
        Route::post('/update', 'ApiController@updateUser');
        Route::post('/remove', 'ApiController@removeUser');
        Route::post('/revive', 'ApiController@reviveUser');
    
        //friendship routes
        Route::any('/add-friends', 'ApiController@addFriends');
        Route::any('/accept-friends', 'ApiController@acceptFriendRequest');
        Route::any('/reject-friends', 'ApiController@rejectFriendRequest');
        Route::any('/unfriend', 'ApiController@unFriend');
        Route::any('/get-my-friend-list', 'ApiController@getFriends');
        Route::any('/search', 'ApiController@searchUser');
        Route::any('/has-friend-request-from', 'ApiController@hasFriendRequestFrom');
        Route::any('/get-friend-requests', 'ApiController@getFriendRequest');

        Route::post('/add-friends-bulk', 'ApiController@addFriendsBulk');
        Route::post('/remove-friends', 'ApiController@removeFriends');
        Route::post('/resync-friends-list','ApiController@resyncFriendLists');
    
    
        Route::POST('/get-unread-messages-count', 'ApiController@getUnreadMessagesCount');
        Route::get('/get-unread-messages-count/{appid}/{id}', 'ApiController@getUnreadMessagesCount');
    });

    //chat routes
    Route::any('/chat/messages','ApiController@getChatMessages');
    Route::post('/chat/save/messages','ApiController@saveChat');

    // PHASE # 2

    Route::group(['prefix' => 'meetup'], function(){
        Route::post('create', 'MeetupController@create');

    });

});






