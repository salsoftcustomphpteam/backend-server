<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();
	Route::get('/', function(){
	    return view('welcome');
	});

Route::group(['middleware' => 'auth'], function(){
	Route::view('/create-application', 'pages.applications.create');
	Route::view('/documentation', 'welcome');

	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/my-application', 'HomeController@myApplication');
	Route::get('/application/edit/{id}', 'HomeController@editApplication');
	Route::get('/application/detail/{id}', 'HomeController@detailApplication');



	Route::post('application/create-application','HomeController@submitApplication');
	Route::post('application/update-application','HomeController@updateApplication');
});


Route::get('testsocket', function(){
    
    return \App\Core\Soachat::emitEvent('REGISTRATION','ADMIN','Andy has registered');
    return 'socket is emitted';
    
});



