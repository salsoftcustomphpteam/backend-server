var express = require('express');
var bodyParser = require('body-parser')
var app = express();
var fs = require('fs');
var path = require('path');
var https = require("http"); // http server core module
var socketIo = require('socket.io');
const crypto = require('crypto');

function resolveURL(url) {
    var isWin = !!process.platform.match(/^win/);
    if (!isWin) return url;
    return url.replace(/\//g, '\\');
}



app.use(express.static(__dirname));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}))

// var ssl_key = fs.readFileSync(path.join(__dirname, resolveURL('ssl/d45f8_ce5a7_131102f28a8747a5bdc1a2978141788e.key')));
// var ssl_cert = fs.readFileSync(path.join(__dirname, resolveURL('ssl/_wildcard__onlinetestingserver_com_d45f8_ce5a7_1568241882_bfc2d7710bb8100067bb6290c83d719a.crt')));

// var ssl_key = fs.readFileSync(path.join(__dirname, resolveURL('ssl/privatekey.pem'))).toString();
// var ssl_cert = fs.readFileSync(path.join(__dirname, resolveURL('ssl/certificate.pem'))).toString();
// var ssl_cabundle = null;


// var options = {
//     key: ssl_key,
//     cert: ssl_cert,
//     ca: ssl_cabundle
// };


//var webServer = https.createServer(options, app);
var webServer = https.createServer(app);

// Start Socket.io so it attaches itself to Express server
var socketServer = socketIo.listen(webServer, {
    "log level": 1
});

app.get('/', (req, res) => {
    res.send('Chat API v.1.0.0');
})


var clients = [];

socketServer.on('connection', function (socket) {
    //socketServer.emit('socket', socket);
    console.log('a user connected');

    socket.on('login', function(data){
        console.log('i am here');
        var user_id = data.uid;
        var appid = data.appid;
        clients.push(socket.id); 
        console.log(`a user ${data.uid}  with socket ID ${socket.id} is connected`);
        socketServer.emit('online',data);
    });

    socket.on('disconnect', function(){
        console.log(`a user with socket ID ${socket.id} is disconnected`);
        clients.splice(clients.indexOf(socket.id), 1);
    });
    
    
    socket.on('typingstatus', function (data) {
        if(data){
            console.log('User is typing')
        }else{
            console.log('User stopped typing')
        }
        socketServer.emit('typingstatus', data);
    });

    socket.on('general_channel', function(data){
	    socketServer.emit('general_channel', data);
    });

    socket.on('soa_chat', function (data) {
        socketServer.emit('soa_chat', data);
    });    
    
});

//listen on port 8080
webServer.listen(5555, function() {
    console.log('listening on https://dev71.onlinetestingserver.com:5555');
});
